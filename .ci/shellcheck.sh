#!/bin/sh -e
# Copyright 2022 Oliver Smith, Dylan Van Assche
# SPDX-License-Identifier: MIT

DIR="$(cd "$(dirname "$0")" && pwd -P)"
cd "$DIR/.."

sh_files="$(find . -name '*.sh')"

for file in $sh_files; do
	echo "Test with shellcheck: $file"
	cd "$DIR/../$(dirname "$file")"
	shellcheck "$(basename "$file")"
done
